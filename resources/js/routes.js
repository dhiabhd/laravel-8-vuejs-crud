import AllPost from "./AllPost";
import AddPost from "./AddPost";
import EditPost from "./EditPost";

export const routes = [
    {
        name: 'home',
        path: '/',
        component: AllPost
    },
    {
        name: 'add',
        path: '/add',
        component: AddPost
    },
    {
        name: 'edit',
        path: '/edit/:id',
        component: EditPost
    }
];
